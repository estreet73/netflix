import Header from "./component/header";
import FilmPrincipal from "./component/filmPrincipal";
import Suggestion from "./component/suggestion";
import Footer from "./component/footer";

function Serie() {
  return (
    <div className="accueil">
      <Header />
      <div>
        <FilmPrincipal genre="tv" />
      </div>
      <div className="tvPop">
        <Suggestion genre="tv" option="top_rated" />
      </div>
      <div className="moviePop">
        <Suggestion genre="tv" option="airing_today" />
      </div>

      <div className="foot">
        <Footer />
      </div>
    </div>
  );
}

export default Serie;
