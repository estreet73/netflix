import Login from "./login/login";
import Accueil from "./accueil";
import Serie from "./serie";
import Film from "./film";
import MaListe from "./maListe";
import Inscription from "./login/inscription";
import { Routes, Route } from "react-router-dom";
import "./App.css";
import { createContext, useState } from "react";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "./firebase.config";

export const Appcontext = createContext();

function App() {
  const [liste, setListe] = useState([]);
  const [check, setCheck] = useState([]);
  const [connect, setConnect] = useState("");

  onAuthStateChanged(auth, (user) => {
    if (user) {
      setConnect(user.email);

      // ...
    } else {
      setConnect("");
    }
  });

  return (
    <Appcontext.Provider value={{ liste, setListe, check, setCheck, connect }}>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/inscription" element={<Inscription />} />
        <Route path="/accueil" element={<Accueil />} />
        <Route path="/serie" element={<Serie />} />
        <Route path="/film" element={<Film />} />
        <Route path="/liste" element={<MaListe />} />
      </Routes>
    </Appcontext.Provider>
  );
}

export default App;
