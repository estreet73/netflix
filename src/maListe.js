import Header from "./component/header";
import { Appcontext } from "./App";
import { useContext } from "react";
import Favoris from "./component/favoris";

function MaListe() {
  const context = useContext(Appcontext);
  const { liste } = context;

  return (
    <div>
      <div className="accueil">
        <Header />
      </div>

      <div className="liste">
        {liste.length < 1 ? (
          <span style={{ color: "white", marginLeft: "40%", fontSize: "20px" }}>
            Ajoutes tes films preferes !!!
          </span>
        ) : null}
        {liste.map((element) => (
          <Favoris genre={element.type} option={element.id} />
        ))}
      </div>
    </div>
  );
}

export default MaListe;
