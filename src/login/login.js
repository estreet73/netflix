import "./login.css";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { useNavigate, Link } from "react-router-dom";
import Footer from "../component/footer";
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../firebase.config";
import { useState } from "react";

const SignupSchema = Yup.object().shape({
  password: Yup.string()
    .min(6, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  email: Yup.string().email("Invalid email").required("Required"),
});

function Login() {
  const nav = useNavigate();
  const [logError, setLogError] = useState(false);
  return (
    <div className="fond">
      <div className="logo">
        <img
          src="https://image.tmdb.org/t/p/original/wwemzKWzjKYJFfCeiB57q3r4Bcm.svg"
          alt="logo"
          height="50px"
          width="200px"
        />
      </div>
      <div className="formulaire">
        <Formik
          initialValues={{
            password: "",
            email: "",
          }}
          validationSchema={SignupSchema}
          onSubmit={(values) => {
            // same shape as initial values
            signInWithEmailAndPassword(auth, values.email, values.password)
              .then((userCredential) => {
                if (userCredential.user) {
                  nav("/accueil");
                }
              })
              .catch((error) => {
                setLogError(true);
              });
          }}
        >
          {({ errors, touched }) => (
            <Form className="form">
              <h1>S identifier</h1>
              <div className="opac">
                <Field
                  className="field"
                  type="password"
                  name="password"
                  placeholder="mot de passe"
                />
                {errors.password && touched.password ? (
                  <div className="error">{errors.password}</div>
                ) : null}
                <Field
                  className="field"
                  name="email"
                  type="email"
                  placeholder="email"
                />
                {errors.email && touched.email ? (
                  <div className="error">{errors.email}</div>
                ) : null}
                <button className="loginSubmit" type="submit">
                  S identifier
                </button>
                {logError ? (
                  <div className="error">
                    les identifiants ne correspondent pas !!
                  </div>
                ) : null}
                <Link style={{ textDecoration: "none" }} to="/inscription">
                  <p className="lienInscription">
                    Pas encore inscrit ? Crees ton compte ici!{" "}
                  </p>
                </Link>
              </div>
            </Form>
          )}
        </Formik>
      </div>

      <Footer />
    </div>
  );
}

export default Login;
