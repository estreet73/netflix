import "./login.css";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { useNavigate } from "react-router-dom";
import Footer from "../component/footer";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from "../firebase.config";
import { useState } from "react";

const SignupSchema = Yup.object().shape({
  nom: Yup.string()
    .min(4, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  prenom: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  password: Yup.string()
    .min(6, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  email: Yup.string().email("Invalid email").required("Required"),
});

function Inscription() {
  const nav = useNavigate();
  const [mailIdentique, setMailIdentique] = useState(false);
  const signUp = (email, password) =>
    createUserWithEmailAndPassword(auth, email, password);

  return (
    <div className="fond">
      <div className="logo">
        <img
          src="https://image.tmdb.org/t/p/original/wwemzKWzjKYJFfCeiB57q3r4Bcm.svg"
          alt="logo"
          height="50px"
          width="200px"
        />
      </div>
      <div className="formulaire">
        <Formik
          initialValues={{
            nom: "",
            prenom: "",
            password: "",
            email: "",
          }}
          validationSchema={SignupSchema}
          onSubmit={async (values) => {
            // same shape as initial values

            try {
              const user = await signUp(values.email, values.password);
              if (user) {
                nav("/");
              }
            } catch (err) {
              console.dir(err);
              if (err.code === "auth/email-already-in-use") {
                setMailIdentique(true);
              }
            }
          }}
        >
          {({ errors, touched }) => (
            <Form className="form">
              <h1>S inscrire</h1>
              <div className="opac">
                <Field
                  className="field"
                  type="firstname"
                  name="prenom"
                  placeholder="prenom"
                />
                {errors.prenom && touched.prenom ? (
                  <div className="error">{errors.prenom}</div>
                ) : null}
                <Field
                  className="field"
                  type="name"
                  name="nom"
                  placeholder="nom"
                />
                {errors.nom && touched.nom ? (
                  <div className="error">{errors.nom}</div>
                ) : null}
                <Field
                  className="field"
                  type="password"
                  name="password"
                  placeholder="mot de passe"
                />
                {errors.password && touched.password ? (
                  <div className="error">{errors.password}</div>
                ) : null}
                {mailIdentique ? (
                  <div className="error">Email deja utilise !</div>
                ) : null}
                <Field
                  className="field"
                  name="email"
                  type="email"
                  placeholder="email"
                />
                {errors.email && touched.email ? (
                  <div className="error">{errors.email}</div>
                ) : null}
                <button className="loginSubmit" type="submit">
                  S inscrire
                </button>
              </div>
            </Form>
          )}
        </Formik>
      </div>

      <Footer />
    </div>
  );
}

export default Inscription;
