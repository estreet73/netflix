import axios from "axios";
import Header from "./component/header";
import FilmPrincipal from "./component/filmPrincipal";
import Footer from "./component/footer";
import Suggestion from "./component/suggestion";

function Accueil() {
  axios(
    `https://api.themoviedb.org/3/search/haw?api_key=d92ed6733f83321852a1e5c5614d4f46`
  ).then((data) => console.log(data));
  return (
    <div className="accueil">
      <Header />
      <div className="photo">
        <FilmPrincipal genre="all" sequence="week" />
      </div>
      <div className="tvPop">
        <Suggestion genre="tv" option="popular" />
      </div>
      <div className="moviePop">
        <Suggestion genre="movie" option="popular" />
      </div>

      <div className="foot">
        <Footer />
      </div>
    </div>
  );
}

export default Accueil;
