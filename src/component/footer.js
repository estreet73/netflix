import "./footer.css";

function Footer() {
  return (
    <div className="footer">
      <p>Des questions ? Appeler le 0805-543-063</p>
      <div className="blocG">
        <div className="bloc">
          <span>FAQ</span>
          <span>Préférences de cookies</span>
        </div>
        <div className="bloc">
          <span>Centre d aide</span>
          <span>Mentions légales</span>
        </div>
        <div className="bloc">
          <span>Conditions d utilisations</span>
        </div>
        <div className="bloc">
          <span>Confidentialité</span>
        </div>
      </div>
    </div>
  );
}

export default Footer;
