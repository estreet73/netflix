import { useEffect, useState } from "react";
import axios from "axios";

function Favoris({ genre, option }) {
  const [fav, setFav] = useState("");
  console.log(fav);
  useEffect(() => {
    axios(
      `https://api.themoviedb.org/3/${genre}/${option}?api_key=d92ed6733f83321852a1e5c5614d4f46`
    )
      .then((data) => setFav(data.data))
      .catch((err) => console.log(err));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div className="card">
      <img
        style={{ height: "auto", width: "100%" }}
        src={`https://image.tmdb.org/t/p/original/${fav.poster_path}`}
        alt=""
      />
    </div>
  );
}

export default Favoris;
