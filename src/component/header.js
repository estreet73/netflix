import { Link, useNavigate } from "react-router-dom";
import { Appcontext } from "../App";
import { useContext } from "react";
import "./header.css";
import { signOut } from "firebase/auth";
import { auth } from "../firebase.config";

function Header() {
  const context = useContext(Appcontext);
  const { connect } = context;
  const nav = useNavigate();

  const deconnect = () => {
    signOut(auth);
    nav("/");
  };

  return (
    <div
      style={{
        backgroundColor: "black",
        height: "40px",
        width: "100%",
        position: "fixed",
        paddingTop: "10px",
        paddingLeft: "30px",
        paddingRight: "30px",
        display: "flex",
        zIndex: 3,
        flexDirection: "row",
      }}
    >
      <div>
        <img
          src="https://image.tmdb.org/t/p/original/wwemzKWzjKYJFfCeiB57q3r4Bcm.svg"
          alt="logo"
          height="20px"
          width="100px"
        />
      </div>
      <div>
        <Link className="link" to={"/accueil/"}>
          Accueil
        </Link>
      </div>
      <div>
        <Link className="link" to={"/serie/"}>
          Series
        </Link>
      </div>
      <div>
        <Link className="link" to={"/film/"}>
          Films
        </Link>
      </div>
      <div>
        <Link className="link" to={"/liste/"}>
          Ma liste
        </Link>
      </div>
      <div className="connect">
        <div>{connect}</div>
        <button className="deconnect" onClick={deconnect}>
          Deconnection
        </button>
      </div>
    </div>
  );
}

export default Header;
