import { useEffect, useState, useContext, forwardRef } from "react";
import axios from "axios";
import "./suggestion.css";
import { Appcontext } from "../App";
import { Snackbar } from "@mui/material";
import MuiAlert from "@mui/material/Alert";
import { Slide } from "@mui/material";
import { Skeleton } from "@mui/material";

const Alert = forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

function TransitionLeft(props) {
  return <Slide {...props} direction="left" />;
}

function Suggestion({ genre, option }) {
  const [film, setFilm] = useState([]);
  const [open, setOpen] = useState(false);
  const [close, setClose] = useState(false);
  const [status, setStatus] = useState("idle");
  const context = useContext(Appcontext);
  const { liste, setListe, check, setCheck } = context;
  const skelet = [1, 2, 3, 4, 5, 6, 7, 8];
  console.log(film);
  film.splice(8);
  const handleClick = (id, type) => {
    setListe([...liste, { id, type }]);
    setCheck([...check, id]);
    setOpen(true);
  };
  const handleClickSupp = (id) => {
    let suppListe = liste.filter((res) => res.id !== id);
    setListe(suppListe);
    let checkSupp = check.filter((res) => res !== id);
    setCheck(checkSupp);
    setClose(true);
  };

  useEffect(() => {
    setStatus("loading");
    axios(
      `https://api.themoviedb.org/3/${genre}/${option}?api_key=d92ed6733f83321852a1e5c5614d4f46`
    )
      .then((data) => {
        setFilm(data.data.results);
        setStatus("done");
      })
      .catch((err) => {
        console.log(err);
        setStatus("error");
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div>
      <p className="titleSugg">
        {genre} {option}
      </p>

      <div className="row">
        {status === "loading"
          ? skelet.map((element) => (
              <Skeleton
                style={{ marginLeft: "10px" }}
                key={element}
                sx={{ bgcolor: "grey.900" }}
                variant="rectangular"
                width={150}
                height={200}
              />
            ))
          : null}
        {status === "error" ? (
          <div>
            <Alert severity="error" sx={{ width: "100%" }}>
              Le cine est ferme !!!
            </Alert>
          </div>
        ) : null}

        {status === "done"
          ? film.map((res) => (
              <div key={res.id} className="card">
                <img
                  style={{ height: "auto", width: "100%" }}
                  src={`https://image.tmdb.org/t/p/original/${res.poster_path}`}
                  alt=""
                />
                {check.includes(res.id) ? (
                  <button
                    onClick={() => {
                      handleClickSupp(res.id);
                    }}
                    className="ajout"
                  >
                    Retirer
                  </button>
                ) : (
                  <button
                    onClick={() => {
                      handleClick(res.id, genre);
                    }}
                    className="ajout"
                  >
                    Ajouter
                  </button>
                )}
              </div>
            ))
          : null}
      </div>
      {open ? (
        <Snackbar
          TransitionComponent={TransitionLeft}
          open={open}
          autoHideDuration={1200}
          onClose={() => setOpen(false)}
        >
          <Alert severity="error" sx={{ width: "100%" }}>
            ajouter a ma liste!
          </Alert>
        </Snackbar>
      ) : null}
      {close ? (
        <Snackbar
          TransitionComponent={TransitionLeft}
          open={close}
          autoHideDuration={1200}
          onClose={() => setClose(false)}
        >
          <Alert severity="error" sx={{ width: "100%" }}>
            retirer de ma liste!
          </Alert>
        </Snackbar>
      ) : null}
    </div>
  );
}

export default Suggestion;
