import axios from "axios";
import { useEffect, useState } from "react";
import "./filmPrincipal.css";
import { Skeleton } from "@mui/material";

function FilmPrincipal({ genre, sequence = "day" }) {
  const [affiche, setAffiche] = useState("");
  const [title, setTitle] = useState("");
  const [status, setStatus] = useState("idle");
  console.log(affiche);

  useEffect(() => {
    if (genre === "movie") {
      setTitle(affiche.title);
    }
    if (genre === "tv") {
      setTitle(affiche.original_name);
    }
    if (genre === "all") setTitle(affiche.title || affiche.original_name);
  }, [affiche.original_name, affiche.title, genre]);

  useEffect(() => {
    setStatus("loading");
    axios(
      `https://api.themoviedb.org/3/trending/${genre}/${sequence}?api_key=d92ed6733f83321852a1e5c5614d4f46`
    )
      .then((data) => {
        setAffiche(data.data.results[0]);
        setStatus("done");
      })
      .catch((err) => setStatus("error"));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {status === "loading" ? (
        <div>
          <Skeleton
            sx={{ bgcolor: "grey.900" }}
            variant="rectangular"
            width={{ width: "100%" }}
            height={850}
          />
        </div>
      ) : null}
      {status === "done" ? (
        <div
          style={{
            backgroundImage: `url(https://image.tmdb.org/t/p/original/${affiche.backdrop_path})`,
            position: "static",
            backgroundPosition: "center",
            backgroundSize: "cover",
            objectFit: "contain",
            width: "100%",
            height: "850px",
          }}
        >
          <div className="title">{title}</div>
          <div className="descript">{affiche.overview}</div>
        </div>
      ) : null}
    </>
  );
}

export default FilmPrincipal;
