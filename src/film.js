import Header from "./component/header";
import FilmPrincipal from "./component/filmPrincipal";
import Suggestion from "./component/suggestion";
import Footer from "./component/footer";

function Film() {
  return (
    <div className="accueil">
      <Header />
      <div>
        <FilmPrincipal genre="movie" />
      </div>
      <div className="tvPop">
        <Suggestion genre="movie" option="top_rated" />
      </div>
      <div className="moviePop">
        <Suggestion genre="movie" option="upcoming" />
      </div>

      <div className="foot">
        <Footer />
      </div>
    </div>
  );
}

export default Film;
